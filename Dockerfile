FROM java:7
ADD target/sb-hello-0.1.0.war /data/sb-hello-0.1.0.war
CMD java -jar -Djava.security.egd=file:/dev/./urandom /data/sb-hello-0.1.0.war