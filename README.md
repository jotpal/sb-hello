# README #

Spring Boot simple war deployment using Jenkins CI with github/bitbucket using Docker

### What is this repository for? ###
2 Jenkins Jobs configured -

1. Job 1. Runs simple test to check if application is up and running inside the container.

2. Job 2. Runs integration-tests against the app running inside the docker container.

### How do I get set up? ###
* Job 1. - jenkins-config-integration-test.xml
* Job 2. - integration-tests-config.xml
